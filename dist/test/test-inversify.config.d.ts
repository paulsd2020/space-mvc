import "reflect-metadata";
import { Container } from "inversify";
import { StoreDefinition } from "../src/dto/store-definition";
export declare function initTestContainer(container: Container, storeDefinitions: StoreDefinition[]): Promise<Container>;
export declare function getTestContainer(): Promise<Container>;
