import { Container } from "inversify";
import { Framework7Params } from "framework7/components/app/app-class";
export declare function buildContainer(container: Container, config: Framework7Params): Container;
