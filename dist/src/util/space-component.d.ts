import { Component } from 'framework7';
import { Router } from 'framework7/modules/router/router';
declare class SpaceComponent extends Component {
    getApp(): any;
    private getRoutingService;
    navigate(navigateParams: Router.NavigateParameters, routeOptions?: Router.RouteOptions, viewName?: string): void;
    submitForm(e: Event, formId: string): void;
}
export { SpaceComponent };
