import { WalletDao } from "../dao/wallet-dao";
import { UiService } from "./ui-service";
declare class WalletService {
    private walletDao;
    private uiService;
    skipBinding: boolean;
    provider: any;
    wallet: any;
    constructor(walletDao: WalletDao, uiService: UiService);
    getClassName(): string;
    init(): Promise<void>;
    createWallet(password: any): Promise<string>;
    connectWallet(wallet: any): Promise<void>;
    unlockWallet(password: string): Promise<void>;
    restoreWallet(recoverySeed: string, password: string): Promise<any>;
    getWallet(): Promise<any>;
    logout(): void;
}
export { WalletService };
