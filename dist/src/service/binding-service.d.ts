import { Container } from "inversify";
declare class BindingService {
    initBindings(container: Container): Promise<void>;
    private _getInitializableBindings;
    private _getServiceIdentifiers;
    private _isInitializable;
    private _isSkipBinding;
}
interface BindingInfo {
    getClassName(): string;
}
export { BindingService, BindingInfo };
