import { BindingInfo } from "./binding-service";
declare class IpfsService implements BindingInfo {
    private ipfsOptions;
    skipBinding: boolean;
    ipfs: any;
    constructor(ipfsOptions: any);
    getClassName(): string;
    init(): Promise<void>;
}
export { IpfsService };
