import { WalletService } from "../service/wallet-service";
import { UiService } from "../service/ui-service";
import { ModelView } from "../util/model-view";
import { RoutingService } from "../service/routing-service";
import { StoreDefinition } from "../dto/store-definition";
declare class WalletController {
    private walletService;
    private uiService;
    private routingService;
    private ownerAddress;
    private storeDefinitions;
    constructor(walletService: WalletService, uiService: UiService, routingService: RoutingService, ownerAddress: string, storeDefinitions: StoreDefinition[]);
    private _initApp;
    showLanding(): Promise<ModelView>;
    showCreateWallet(): Promise<ModelView>;
    createWallet(): Promise<ModelView>;
    createWalletSuccess(): Promise<ModelView>;
    createWalletDone(): Promise<ModelView>;
    unlockWallet(): Promise<ModelView>;
    showEnterRecovery(): Promise<ModelView>;
    restoreButtonClick(): Promise<ModelView>;
}
export { WalletController };
