interface Initializable {
    init(): void;
}
export { Initializable };
