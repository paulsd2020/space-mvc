import { merge } from 'webpack-merge'
import { web, node }from './webpack.common'

let config = {
  mode: 'development',
  devtool: 'inline-source-map',
  watch: true
}

let mergedWeb = merge(web, config)
let mergedNode = merge(node, config)

export default [mergedWeb, mergedNode]