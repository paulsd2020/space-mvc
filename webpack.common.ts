const path = require('path');
const nodeExternals = require('webpack-node-externals');


const babelLoader = {
  loader: 'babel-loader',
  options: {
    cacheDirectory: false,
    presets: ['@babel/preset-env']
  }
}


const framework7ComponentLoader = {
  loader: 'framework7-component-loader-spacemvc',
  options: {
    helpersPath: './src/template7-helpers-list.js',
    partialsPath: './src/pages/',
    partialsExt: '.f7p.html'
  }
}



let web = {
  name: "web",
  entry: './src/index.ts',
  module: {
    rules: [
      {
        test: /\.js$/,
        use: [babelLoader]
      },
      {
        test: /\.tsx?$/,
        exclude: '/node_modules/',
        loader: 'ts-loader',
      },
      {
        test: /\.f7.html$/,
        use: [babelLoader, framework7ComponentLoader],
      }
    ],
  },
  resolve: {
    extensions: ['*', '.js', '.jsx', '.tsx', '.ts'],
  },
  output: {
    filename: 'index-browser.js',
    libraryTarget: 'umd',
    globalObject: 'this',
    path: path.resolve(__dirname, 'dist'),
  }
}



let node = { ...web, ...{
  name: "node",
  target: "node",
  // entry: './src/node.ts',
  externals: [nodeExternals()],
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        exclude: '/node_modules/',
        loader: 'ts-loader',
      },
      {
        test: /\.f7.html$/,
        use: [babelLoader, framework7ComponentLoader],
      }
    ],
  },
  output: {
    filename: 'index-node.js',
    libraryTarget: 'umd',
    path: path.resolve(__dirname, 'dist'),
  }
}}



export {
  web,
  node
}


