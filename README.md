# Space MVC

A TypeScript toolset to simplify building peer-to-peer applications using IPFS, Ethereum, and OrbitDb. 

## Our primary goals

* Provide a radically faster getting started experience for P2P app development.
* Make opinionated decisions to reduce boilerplate code.
* Implements simple, familiar MVC pattern.  
* Serverless. In the real sense. There's no server.  
* Easy to swap out new crypto technology. It changes so quickly.
* Apps work offline by default. Data is primarily stored in the user's browser. 

SpaceMVC provides a pre-configured development environment with reasonable defaults. Start building real apps immediately. 

### Maintainable and Testable
* Object-oriented design and [Inversify](http://inversify.io/) dependency injection helps you decouple your business logic and your UI to write maintainable and testable code.  
* SpaceMVC relies heavily on [Framework7](https://framework7.io/) and the answers to many questions can be found in their excellent documentation. You can use it to build full featured iOS, Android & Desktop apps that look and feel native. It uses nothing but HTML, CSS, and Javascript.

### Important Technology 
* [Typescript](https://www.typescriptlang.org/) - TypeScript is a typed superset of JavaScript that compiles to plain JavaScript.
* [Framework7](https://framework7.io/) - Build full featured iOS, Android & Desktop apps with HTML, CSS, and Javascript
* [IPFS](https://ipfs.io) - A peer-to-peer hypermedia protocol
designed to make the web faster, safer, and more open.
* [OrbitDB](https://github.com/orbitdb/orbit-db) - OrbitDB is a serverless, distributed, peer-to-peer database
* [The Ethers Project](https://github.com/ethers-io/ethers.js) - A complete Ethereum wallet implementation and utilities in JavaScript (and TypeScript).
* [Inversify](http://inversify.io/) - IoC container Inversify.

## Getting Started 

The fastest way to create a project is to clone the [TRY University](https://gitlab.com/ptoner/try-university) project. //TODO: this will become a truffle box or similar

## Installation

Add the following to your package.json

```javascript
  "dependencies": {
    "space-mvc": "0.0.15-beta"
  }
```

## Initialization

* Create Framework 7 [parameters](https://framework7.io/docs/app.html).
* Define OrbitDb data storage schema. 
* Set up Inversify container with dependencies.

```typescript

    import "core-js/stable"
    import "regenerator-runtime/runtime"

    import SpaceMVC from "space-mvc";

    import { container } from "./inversify.config"

    let f7Params = {
        root: '#app', // App root element
        id: 'example', // App bundle ID
        name: 'SpaceMVC Example', // App name
        theme: 'auto', // Automatic theme detection
    }

    let schema = [{
        name: "person", type: "mfsstore", load: 100, schema: {
            id: { unique: true },
            firstName: { unique: false },
            lastName: { unique: false }
        }
    }]

    await SpaceMVC.init( container, f7Params, "0x88bfEE1f69e83aa5F146B2B6F527878866D39fE9", schema)

```


# API

SpaceMVC exports the following classes.

* ModelView
* QueueService
* PagingService
* PagingViewModel
* UiService
* WalletService
  * InvalidWalletException
* SchemaService
* IpfsService
* OrbitService
* RoutingService
* WalletController
* StoreDefinition

* PromiseView
* Schema
* Container
* SpaceComponent
* inject
* service
* controller
* dao
* Initializable
* routeMap
* initTestContainer

## Events

* spacemvc:initFinish - Fired when SpaceMVC has finished initializing.


# Build

Production:

```console
npm run build
```

Development:

```console
npm run build:dev
```

Tests:

```console
npm run test
```


* dist/index-browser.js - Browser build
* dist/index-node.js - Node build




