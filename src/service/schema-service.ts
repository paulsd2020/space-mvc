import { OrbitService } from "./orbit-service";
import { IpfsService } from "./ipfs-service";
import service from "../decorators/service";
import { LoadedSchema, Schema } from "../dto/schema";
import { StoreDefinition } from "../dto/store-definition";

const WALLET_SCHEMA = "WALLET_SCHEMA"


@service()
class SchemaService {

    public skipBinding = true

    public loadedSchemas: {
        [key:string]:LoadedSchema
    } = {}

    constructor(
        private ipfsService:IpfsService,
        private orbitService:OrbitService
    ) {}

    /**
     * Creates a Schema object from the passed parameters and stores it in IPFS. Returns the Schema. 
     * 
     * @param ownerAddress 
     * @param storeDefinitions 
     */
    async create(ownerAddress: string, storeDefinitions:StoreDefinition[]): Promise<Schema> {

        let schema: Schema = {
            addresses: {},
            storeDefinitions: storeDefinitions
        }

        let defaultOptions = {
            create: true,
            accessController: {
                write: [ownerAddress]
            }
        }

        if (storeDefinitions) {
            for (let store of storeDefinitions) {

                // let createdStore
    
                let storeName = `${store.name}-${ownerAddress.toLowerCase()}`
    
                let createdStore 

                switch (store.type) {
                    case "kvstore":
                        createdStore = await this.orbitService.orbitDb.kvstore(storeName, defaultOptions)
                        break
    
                    case "docs":
                        createdStore = await this.orbitService.orbitDb.docs(storeName, defaultOptions)
                        break
    
                    case "mfsstore":
                        let options = JSON.parse(JSON.stringify(defaultOptions))
                        options.type = "mfsstore"
                        options.schema = store.schema
    
                        createdStore = await this.orbitService.orbitDb.open(storeName, options)
    
                        break
    
                }
    
                schema.addresses[store.name] = createdStore.address.toString()
    
            }
        }

        schema.owner = ownerAddress


        //Write global schema to IPFS
        let buffer = Buffer.from(JSON.stringify(schema))
        let cid = await this.ipfsService.ipfs.object.put(buffer)


        schema.cid = cid.toString()

        return schema

    }

    /**
     * Loads Schema from IPFS based on cid
     * @param cid 
     */
    async read(cid: string): Promise<Schema> {

        let loaded = await this.ipfsService.ipfs.object.get(cid)

        let data = loaded.Data ? loaded.Data.toString() : loaded.data.toString()

        let schema = JSON.parse(data)

        schema.cid = cid

        return schema

    }

    /**
     * Takes a Schema object and then loads all the orbit stores associated with it. 
     * 
     * Reads the storeDefinitions from the schema and then calls load() on the orbit store.
     * 
     * @param key 
     * @param schema 
     */
    async load(schema: Schema) {

        let loadedSchema:LoadedSchema = {
            stores: {},
            schema: schema
        }


        for (let field in schema.addresses) {

            loadedSchema.stores[field] = await this.orbitService.orbitDb.open(schema.addresses[field])

            let definition = schema.storeDefinitions.filter(def => def.name == field)[0]

            if (definition.load) {
                console.log(`Loading ${field}`)

                await loadedSchema.stores[field].load(definition.load)
            } else {
                console.log(`Skipping ${field}`)
            }
        }

        this.loadedSchemas[WALLET_SCHEMA] = loadedSchema


    }


    getStore(storeName: string) {
        return this.loadedSchemas[WALLET_SCHEMA].stores[storeName]
    }

    async getSchemaInfo() {
        
        let stores = this.loadedSchemas[WALLET_SCHEMA].stores

        let translated = []
        for (let key in stores) {
            
            let store = stores[key]
            let count = await store.count()

            translated.push({
                name: key,
                address: store.address.toString(),
                loaded: count
            })
        }

        //Get a copy of the global schema
        let globalSchema:Schema = await this.loadedSchemas[WALLET_SCHEMA].schema

        return {
            stores: translated,
            globalSchema: globalSchema
        }

    }

    

}



export {
    SchemaService, Schema, StoreDefinition 
}
