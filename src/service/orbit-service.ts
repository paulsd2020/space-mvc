import { injectable, inject } from "inversify"
import { SpaceMVC } from "../space-mvc"
import { IdentityService } from "./identity-service"
import { IpfsService } from "./ipfs-service"
import service from "../decorators/service"


let OrbitDB = require('orbit-db')
let Keystore = require('orbit-db-keystore')
let MfsStore = require('orbit-db-mfsstore')

@service()
class OrbitService {
    
    public skipBinding = true
    public orbitDb

    constructor(
        private ipfsService:IpfsService,
        private identityService: IdentityService,
        @inject("orbitDbOptions") private orbitDbOptions
    ) {}

    getClassName(): string {
        return "OrbitService"
    }

    

    async init(wallet?:any) {

        OrbitDB.addDatabaseType("mfsstore", MfsStore)

        if (wallet) {
            let keystore = new Keystore()
            let identity = await this.identityService.getIdentity(keystore, wallet)
            this.orbitDbOptions['identity'] = identity 
        }

        this.orbitDb = await OrbitDB.createInstance(this.ipfsService.ipfs, this.orbitDbOptions)    

    }


}

export {
    OrbitService
}