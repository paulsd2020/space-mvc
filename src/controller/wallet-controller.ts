import { inject } from "inversify"
import { WalletService } from "../service/wallet-service"
import { UiService } from "../service/ui-service"
import { ModelView } from "../util/model-view"

import LandingComponent from '../components/wallet/landing.f7.html'
import EnterRecoveryComponent from '../components/wallet/enter-recovery.f7.html'
import CreateWalletComponent from '../components/wallet/create-wallet.f7.html'
import CreateWalletSuccessComponent from '../components/wallet/create-wallet-success.f7.html'


import { routeMap } from "../decorators/route-map"
import controller from "../decorators/controller"
import { RoutingService } from "../service/routing-service"

import SpaceMVC from ".."
import { StoreDefinition } from "../dto/store-definition"
import { IpfsService } from "../service/ipfs-service"
import service from "../decorators/service"



@controller()
class WalletController {

    constructor(
        private walletService: WalletService,
        private uiService: UiService,
        private routingService: RoutingService,
        @inject("ownerAddress") private ownerAddress:string,
        @inject("storeDefinitions") private storeDefinitions:StoreDefinition[] 
    ) {
    }

    private async _initApp() {

        this.uiService.showSpinner()

        await SpaceMVC.initFinish(this.ownerAddress, this.storeDefinitions)    

        this.uiService.hideSpinner()

    }


    @routeMap("/wallet")
    async showLanding(): Promise<ModelView> {

        return new ModelView(async () => {
            let existingWallet = await this.walletService.getWallet()

            return {
                showUnlock: (existingWallet),
                appName: SpaceMVC.name()
            }

        }, LandingComponent)
    }

    @routeMap("/showCreateWallet")
    async showCreateWallet(): Promise<ModelView> {
        return new ModelView(async () => {
        }, CreateWalletComponent)
    }

    @routeMap("/createWallet")
    async createWallet() {

        return new ModelView(async (formData) => {

            //Create new wallet
            let mnemonic = await this.walletService.createWallet(formData.newPassword)

            this.routingService.navigate({
                path: "/createWalletSuccess"
            },
                {
                    context: {
                        mnemonic: mnemonic
                    }
                }
            )

        })

    }

    @routeMap("/createWalletSuccess")
    async createWalletSuccess() {
        return new ModelView(async (data) => {
            return data
        }, CreateWalletSuccessComponent)
    }

    @routeMap("/createWalletDone")
    async createWalletDone() {
        return new ModelView(async () => {            
            try {
                await this._initApp()
            } catch (ex) {
                console.log(ex)
                this.uiService.hideSpinner()
                this.uiService.showPopup("Unable to initialize app")
            }
        })
    }



    @routeMap("/unlockWallet")
    async unlockWallet() {
        
        return new ModelView(async (formData) => {

            this.uiService.showSpinner('Unlocking wallet...')

            try {
                await this.walletService.unlockWallet(formData.password)
                await this._initApp()
            } catch (ex) {
                console.log(ex)
                this.uiService.hideSpinner()
                this.uiService.showPopup("Incorrect password please try again.")
            }

        })


    }


    @routeMap("/enterRecovery")
    async showEnterRecovery(): Promise<ModelView> {
        return new ModelView(async () => {
            return {}
        }, EnterRecoveryComponent)

    }

    @routeMap("/restoreAccount")
    async restoreButtonClick() {

        return new ModelView(async (formData) => {

            this.uiService.showSpinner('Restoring wallet...')

            let wallet = await this.walletService.restoreWallet(formData.recoverySeed, formData.newPassword)

            if (wallet) {
                await this._initApp()
            } else {
                this.uiService.showPopup("Error restoring wallet. Please try again.")
                this.uiService.hideSpinner()
            }

        })

    }

}


export {
    WalletController
}