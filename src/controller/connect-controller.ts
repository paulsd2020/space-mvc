import { IpfsService } from "../service/ipfs-service";
import { UiService } from "../service/ui-service";
import { ConnectService } from "../service/connect-service";
import { routeMap } from "../decorators/route-map";
import { ModelView } from "../util/model-view";

import ConnectIndexComponent from '../components/connect/index.f7.html'
import controller from "../decorators/controller";
import { ConnectInfo } from "../dto/connect-info";
import { RoutingService } from "../service/routing-service";
import { SchemaService } from "../service/schema-service";


@controller()
class ConnectController {

    constructor(
        private connectService:ConnectService,
        private uiService:UiService,
        private routingService:RoutingService,
        private schemaService:SchemaService
    ) {}

    @routeMap("/connect")
    async index() {

        return new ModelView( async () => {

            let vm = {
                connect: await this.connectService.getConnectInfo(),
                schema: await this.schemaService.getSchemaInfo()
            }

            return vm           
        }, ConnectIndexComponent)

    }

    @routeMap("/connect/addPeer")
    async addPeer() {

        return new ModelView( async (data) => {

            try {
                await this.connectService.addPeer(data.peerAddress)
            } catch(ex) {
                this.uiService.showPopup(ex)
            }
            
            this.routingService.navigate({
                path: "/connect"
            })


        }, ConnectIndexComponent)

    }

}


export {
    ConnectController
}