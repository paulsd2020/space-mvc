interface StoreDefinition {
    name: string
    type: string
    load?: number
    schema?: any
}

export {
    StoreDefinition
}