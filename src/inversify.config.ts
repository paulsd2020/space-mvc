import { Container, interfaces } from "inversify";


import { UiService } from "./service/ui-service";
import { PagingService } from "./service/paging-service";
import { IdentityService } from "./service/identity-service";
import { WalletService } from "./service/wallet-service";
import { SchemaService } from "./service/schema-service";

import { Framework7Params } from "framework7/components/app/app-class";
import { OrbitService } from "./service/orbit-service";
import { IpfsService } from "./service/ipfs-service";
import { WalletDao } from "./dao/wallet-dao";
import { WalletController } from "./controller/wallet-controller";
import { RoutingService } from "./service/routing-service";
import { QueueService } from "./service/queue_service";
import { ConnectController } from "./controller/connect-controller";
import { ConnectService } from "./service/connect-service";
import { BindingService } from "./service/binding-service";




export function buildContainer(container:Container, config:Framework7Params) : Container {

    function framework7() {
        
        const Framework7 = require('framework7/js/framework7.bundle')

        return new Framework7(config)

    }


    container.bind("framework7").toConstantValue(framework7())

    //Default IPFS options if we're not given any
    if (!container.isBound("ipfsOptions")) {

        container.bind("ipfsOptions").toConstantValue(
            {
                repo: `space-mvc-default`,
                EXPERIMENTAL: {
                    ipnsPubsub: true
                },
                preload: {
                    enabled: false
                },
                relay: {
                    enabled: true,
                    hop: {
                        enabled: true // enable circuit relay HOP (make this node a relay)
                    }
                },
                config: {
                    Addresses: {
                        Swarm: []
                    }
                }
            }
        )
    }

    if (!container.isBound("orbitDbOptions")) {
        container.bind("orbitDbOptions").toConstantValue({})
    }

    container.bind(IdentityService).toSelf().inSingletonScope()


    container.bind(WalletDao).toSelf().inSingletonScope()

    container.bind(UiService).toSelf().inSingletonScope()
    container.bind(QueueService).toSelf().inSingletonScope()
    container.bind(PagingService).toSelf().inSingletonScope()
    
    container.bind(SchemaService).toSelf().inSingletonScope()

    container.bind(OrbitService).toSelf().inSingletonScope()
    container.bind(IpfsService).toSelf().inSingletonScope()
    
    container.bind(WalletService).toSelf().inSingletonScope()
    container.bind(WalletController).toSelf().inSingletonScope()

    container.bind(ConnectService).toSelf().inSingletonScope()
    container.bind(ConnectController).toSelf().inSingletonScope()
    
    container.bind(RoutingService).toSelf().inSingletonScope()
    container.bind(BindingService).toSelf().inSingletonScope()
    

    return container
}