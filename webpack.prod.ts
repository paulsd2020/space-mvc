import { merge } from 'webpack-merge'
import { web } from './webpack.common'

export default merge(web, {
    mode: 'production',
    watch: false
})