import assert from 'assert'

import { getTestContainer } from "../test-inversify.config"
import { SchemaService, Schema, StoreDefinition } from '../../src/service/schema-service'




describe('SchemaService', async () => {

    let schemaService: SchemaService

    let storeDefinitions:StoreDefinition[] = [{
        name: "student", type: "mfsstore", load: 100, schema: {
            walletAddress: { unique: true },
            firstName: { unique: false },
            lastName: { unique: false }
        }
    }]
    

    before('Before', async () => {

        let container = await getTestContainer()
        schemaService = container.get(SchemaService)
    })    


    it("should create a schema", async () => {

        //Act
        let schema:Schema = await schemaService.create("owneraddress", storeDefinitions )

        //Assert
        assert.equal(schema.owner, "owneraddress")
        assert.equal(schema.cid, "QmUBJcECp4hahzvPtcy4tSjsfQ9EDraP9wiaEymWuUYX34")
        assert.equal(schema.addresses['student'], '/orbitdb/zdpuAxsdxJKTQZUCJE8EZSb3beU5F1TbegNQxTEFocAUzbUAT/student-owneraddress')
        assert.equal(schema.storeDefinitions.length, 1)
    })

    
    it("should read a schema", async () => {

        //Act
        let schema:Schema = await schemaService.read("QmUBJcECp4hahzvPtcy4tSjsfQ9EDraP9wiaEymWuUYX34" )

        //Assert
        assert.equal(schema.owner, "owneraddress")
        assert.equal(schema.cid, "QmUBJcECp4hahzvPtcy4tSjsfQ9EDraP9wiaEymWuUYX34")
        assert.equal(schema.addresses['student'], '/orbitdb/zdpuAxsdxJKTQZUCJE8EZSb3beU5F1TbegNQxTEFocAUzbUAT/student-owneraddress')
        assert.equal(schema.storeDefinitions.length, 1)

    })

    it("should load a schema", async () => {

        //Arrange
        let schema:Schema = await schemaService.read("QmUBJcECp4hahzvPtcy4tSjsfQ9EDraP9wiaEymWuUYX34" )

        //Act
        await schemaService.load(schema)

        //Assert
        let loadedSchema = schemaService.loadedSchema

        assert.equal(loadedSchema.schema.owner, "owneraddress")
        assert.equal(loadedSchema.schema.cid, "QmUBJcECp4hahzvPtcy4tSjsfQ9EDraP9wiaEymWuUYX34")
        assert.equal(loadedSchema.schema.addresses['student'], '/orbitdb/zdpuAxsdxJKTQZUCJE8EZSb3beU5F1TbegNQxTEFocAUzbUAT/student-owneraddress')
        assert.equal(loadedSchema.schema.storeDefinitions.length, 1)

        assert.equal(await loadedSchema.stores["student"].address.toString(), '/orbitdb/zdpuAxsdxJKTQZUCJE8EZSb3beU5F1TbegNQxTEFocAUzbUAT/student-owneraddress')


    })



})
