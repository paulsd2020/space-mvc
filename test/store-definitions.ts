import { StoreDefinition } from "../src/dto/store-definition"

let storeDefinitions:StoreDefinition[] = [{
    name: "student", type: "mfsstore", load: 100, schema: {
        walletAddress: { unique: true },
        firstName: { unique: false },
        lastName: { unique: false }
    }
}]

export {
    storeDefinitions
}